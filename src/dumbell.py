import torch
import torch.nn as nn
import torch.optim as optim
from torch_geometric.data import Data
from  torch_geometric.utils import degree
from env import *



# # Define a simple neural network for the policy
# class PolicyNetwork(nn.Module):
#     def __init__(self, input_size, output_size):
#         super(PolicyNetwork, self).__init__()
#         self.fc = nn.Linear(input_size, output_size)

#     def forward(self, x):
#         return torch.softmax(self.fc(x), dim=-1)

# # Initialize the policy network
# input_size = 1  # Adjust based on your state representation
# output_size = 1  # Number of possible actions
# policy_net = PolicyNetwork(input_size, output_size)

# # Define the optimizer and loss function
# optimizer = optim.Adam(policy_net.parameters(), lr=0.001)
# criterion = nn.CrossEntropyLoss()

# # Function to select an action using the policy
# def select_action(state):
#     state_tensor = torch.tensor(state, dtype=torch.float).view(1, -1)
#     probabilities = policy_net(state_tensor)
#     action = torch.multinomial(probabilities, 1).item()
#     return action

if __name__ == "__main__":

    x = torch.tensor([], dtype=torch.float)
    edge_index = torch.tensor([[0,1],[1,0],
                            [0,2],[2,0],
                            [2,3],[3,2],
                            [1,3],[3,1],
                            [3,4],[4,3],
                            [4,5],[5,4],
                            [4,6],[6,4],
                            [5,7],[7,5],
                            [6,7],[7,6]], dtype=torch.long)
    y = torch.tensor([0,0,0,0,0,0,0,1], dtype=torch.int)
    train_mask = [False,True,True,True,True,True,True,False]
    val_mask = [False,False,False,False,False,False,False,False]
    test_mask = [True,False,False,False,False,False,False,True]

    data = Data(edge_index=edge_index.t().contiguous(), y=y, train_mask=train_mask, val_mask=val_mask, test_mask=test_mask)
    # print(data.num_nodes)
    print(len(data.edge_index))
    # print(data.degree(data.edge_index[0]))
    print(degree(data.edge_index[0], dtype=torch.long))

    env = GraphEnvironment(data)

    print(env.reward(data))
    # env.render()
    # states = env.observation_space
    # print("states :",states)
    # actions = env.action_space
    # print("action_space :",actions)


    # Create a flag - restart or not
    # done = True
    # # Loop through each frame in the game
    # for step in range(100000): 
    #     # Start the game to begin with 
    #     if done: 
    #         # Start the gamee
    #         env.reset()
    #     # Do random actions
    #     state, reward, done, info = env.step(env.action_space.sample())
    #     # Show the game on the screen
    #     env.render()
    # # Close the game
    # env.close()
