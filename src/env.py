import gym
from gym import spaces
import torch
from torch_geometric.data import Data
from  torch_geometric.utils import degree
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

class GraphEnvironment(gym.Env):
    def __init__(self, original_data : Data):
        super(GraphEnvironment, self).__init__()

        self.state = original_data
        self.current_node = 0
        # self.state = 
        self.action_space = spaces.MultiDiscrete([len(original_data.y), 2])  # ! Two classes (0 or 1)
        self.observation_space = spaces.Tuple([spaces.Discrete(len(original_data.y)), spaces.MultiBinary(len(original_data.y))])

    def reset(self):
        self.current_node = 0
        # self.state = # default
        return
    
    def reward(self, state : Data):
        # print(state.num_edges, state.num_nodes)
        reward = 0
        V_a = 0
        V_b = 0
        # print("entered")
        for i in range(state.num_edges):
            node = state.edge_index[0][i]
            oposite_node = state.edge_index[1][i]
            # print(node , " vs ", oposite_node )
            if state.y[node] != state.y[oposite_node]:
                reward+=1

        degrees = torch.add(degree(state.edge_index[0], dtype=torch.long),degree(state.edge_index[1], dtype=torch.long))
        # print(degrees)
        for i in range(state.num_nodes):
            if state.y[i] == 0:
                V_a += degrees[i]
            else:
                V_b += degrees[i]
            
                # print("reward +1")
        # print("left")
        # print(V_a, V_b)
        return reward * (1/V_a + 1/V_b)

    def step(self, action):
        self.state
        # state, reward, done, info
        self.state = new_state
        return new_state, reward, done, {}


    def render(self, mode='human'):
        G = nx.Graph()

        # Add nodes
        G.add_nodes_from(range(2)) # ! only two classes | must change later

        # Add edges
        for edge in self.state.edge_index.t().contiguous():
            G.add_edge(edge[0].item(), edge[1].item())

        # Set node colors based on the labels (y)
        node_colors = ['red' if label == 0 else 'blue' for label in self.state.y] # ! only two classes | must change later

        # Draw the graph
        pos = nx.spring_layout(G)
        nx.draw(G, pos, with_labels=True, node_color=node_colors, font_color='white', font_weight='bold', font_size=8)
        plt.show()
        pass
