
\documentclass[10pt,journal,compsoc]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{amssymb,amsfonts,amsmath,hyperref,listings,subfigure}
\usepackage{graphicx} % Add if needed
\graphicspath{{./res/}}

\title{Review for Graph Partitioning and Sparse Matrix Ordering using Reinforcement Learning and Graph Neural Networks}
\author{Authors of the article: Alice Gatti, Zhixiong Hu, Tess Smidt, Esmond G. Ng, Pieter Ghysels\\Author of the review: Mohamed Aymane Kherraz}

% \textbf{Index terms}
% % Graph Partitioning, Reinforcement Learning, Graph Neural Networks, Graph convolutional network.

\begin{document}

\maketitle

\begin{abstract}
	This review provides a foundation for delving deeper into the paper's methodology and results. In the next stage, I will focus on a close inspection of the research. This will include studying the reinforcement learning framework and the usage of graph convolutional layers. Additionally, I will attempt to reproduce the reported results for a deeper understanding of the method's effectiveness and potential limitations.
\end{abstract}


\section{Introduction}

Graph partitioning aims to divide a graph into balanced sub-graphs while minimizing the connections between the partitions. This problem is NP-complete \cite{NP-complete}, thus existing solutions rely on approximations and don't fully exploit the full power of modern computing.

The field of machine learning has seen a rapidly growing interest by being applied to solve complex problems like the traveling salesman problem or knapsack problem. These problems, often categorized as NP-complete, are tremendously difficult to solve exactly. Many problems of this nature involve optimizing elements within a network, which can be effectively represented by graphs. The first study was done by Azade Nazi on his paper, GAP: Generalizable approximate graph partitioning Framework \cite{GAP} where he defined the graph partitioning mathematically and formally.


\subsection{Definitions} \label{problem}

Let $G = (V, E)$ be a graph where $V = \{v_i\}$ and $E = \{e(v_i, v_j) \mid v_i \in V, v_j \in V\}$ are the set of nodes and edges in the graph, respectively. Let $n$ be the number of nodes.

A graph $G$ can be partitioned into $g$ disjoint sets $S_1, S_2, \ldots, S_g$, where the union of the nodes in those sets is $V$ $\left( \bigcup_{k=1}^{g} S_k = V \right)$, and each node belongs to only one set $\left( \forall i,j :  S_j \bigcap^{g} S_i = \emptyset \right)$, by simply removing edges connecting those sets.

The total number of edges that are removed from $G$ in order to form disjoint sets is called the cut. Given sets $S_k$ and $\overline{S_k}$ (the complement of $S_k$), the cut $(S_k, \overline{S_k})$ is formally defined as:

\begin{equation}
	\text{cut}(S_k, \overline{S_k}) = \sum_{{v_i \in S_k}, {v_j \in \overline{S_k}}} e(v_i, v_j) \quad
	\label{eq:cut}
\end{equation}

This formula can be generalized to multiple disjoint sets $S_1, S_2, \ldots, S_g$, where $\overline{S_k}$ is the union of all sets except $S_k$.

\begin{equation}
	\text{cut}(S_1, S_2, \ldots, S_g) = \frac{1}{2} \sum_{i=1}^{g} \text{cut}(S_k, \overline{S_k}) \quad
	\label{eq:cut-gen}
\end{equation}


\subsection{Normalized cut}

The optimal partitioning of a graph that minimizes the cut (equation \ref{eq:cut-gen}) is a well-studied problem and there exist efficient polynomial algorithms for solving it \cite{normalized-cut}\cite{Spectralnet}. However, the minimum cut criteria favor cutting nodes whose degree is small and lead to unbalanced sets/partitions.

To avoid such bias, normalized cut (Ncut), which is based on the graph conductance, has been studied by [Shi and Malik, 2000; Zhang and Rohe, 2018] \cite{normalized-cut}\cite{Spectralnet}, where the cost of a cut is computed as a fraction of the total edge connections to all nodes.

\begin{equation}
	\text{Ncut}(S_1, S_2, \ldots, S_g) = \frac{1}{g} \sum_{k=1}^g \frac{\text{cut}(S_k, \overline{S_k})}{\text{vol}(S_k, V)} \quad
	\label{eq:nc-gen}
\end{equation}

Where $\text{vol}(S_k, V) = \sum_{{v_i \in S_k}, {vj \in V}} e(v_i, v_j)$, i.e., total degree of nodes belonging to $S_k$ in graph $G$.


\section{Exploring Graph Neural Networks (GNNs)} \label{gnns}

Finding the best partitioning for a graph directly translates to minimizing the normalized cut (equation~\ref{eq:nc-gen}). This is where Graph Neural Networks (GNNs) come into play. GNNs are powerful learning tools specifically designed to work with graph data, making them well-suited for tackling these challenging combinatorial optimization problems.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=3cm]{graph_4.png}
	\caption{Graph with 4 nodes.}
	\label{fig:graph_4}
\end{figure}

One of the main pillars of GNNs is graph convolution, which simulates the function of convolution on euclidean data on non-euclidean data (Graphs). This generalization is called message passing, and it allows to aggregate the neighbors' information and propagate it on the graph.

\newpage

We consider the graph, figure~\ref{fig:graph_4}, and the computation graph of the node A, figure~\ref{fig:message-passing}. The computation graph holds the recursive neighboring nodes for A to a depth of 2. The mechanism of transferring information from the leaves to the root is called message passing.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=8cm]{compute_A_legend.png}
	\caption{Message passing structure.}
	\label{fig:message-passing}
\end{figure}

Many variations of this message passing skeleton give birth to new convolution layers: GCN \cite{GCN}, SAGE \cite{SAGE}, and GAT \cite{GAT}, by simply changing the aggregation function or adding the attention mechanism. Many computation graphs are created (one per node), and the weights (model parameters) are shared between all MLPs on the same level (Figure ~\ref{fig:share_weight}) which allows generalization when a new node is added.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=9cm]{shared_weights.png}
	\caption{Shared weights in computation graphs.}
	\label{fig:share_weight}
\end{figure}

\subsection{Mathematical definition}
Let

\begin{equation}
	x_{i}^{(k)}=\gamma^{(k)}(x_{i}^{(k-1)},\bigoplus_{j\in N(i)}\phi^{(k)}(x_{i}^{(k-1)},x_{j}^{(k-1)},e_{j,i})),
	\label{eq:math-mess-pass}
\end{equation}

Where,

\begin{itemize}
	\item $x^{(k-1)} \in \mathbb{R}^{num\_node\_features}$ denotes node features of node $i$ in layer $(k-1)$ and $e_{ji} \in \mathbb{R}^{num\_edge\_features}$ denotes (optional) edge features from node $j$ to node $i$.

	\item $\odot$ denotes a differentiable, permutation-invariant function, e.g., sum, mean, or max.

	\item $\gamma$ and $\phi$ denote differentiable functions such as MLPs (Multi-Layer Perceptrons).
\end{itemize}

The general math formula of the message passing mechanism is the equation \ref{eq:math-mess-pass} \cite{geometric}. In order to create a GCN convolution layer, we need to use the \textbf{mean} as an aggregation function. A GCN convolutional network \cite{GCN} is then formally defined as

\begin{equation}
	H^{(l+1)}=\sigma(\tilde{D}^{-\frac{1}{2}}\tilde{A}\tilde{D}^{-\frac{1}{2}}H^{(l)}W^{(l)}),
	\label{eq:gcn-nn}
\end{equation}

where,

\begin{itemize}
	\item $A$ = $A + I_{num\_nodes}$ is the adjacency matrix of the undirected graph $G$ with added self-connections. $I_{num\_nodes}$ is the identity matrix of size $num\_nodes \times num\_nodes$.
	\item $D_{ii} = \sum_j A_{ij}$ is the degree of node $i$ in the graph $G$.
	\item $W^{(1)}$ is a layer-specific trainable weight matrix.
	\item $\sigma(.)$ denotes an activation function, such as the ReLU activation function $\sigma(x) = \text{max}(0, x)$.
	\item $H^{(1)} \in \mathbb{R}^{N\times D}$ is the matrix of activations in the first layer.
	\item $H^{(0)} = X$ is the input feature matrix.
\end{itemize}


\section{Reinforcement Learning for Graph Partitioning} \label{rl4g}

Reinforcement learning is a powerful technique in machine learning where an agent learns to take optimal actions in an environment to achieve a desired goal. Unlike supervised learning where the agent is trained on labeled data with correct answers, reinforcement learning works through a trial-and-error approach. The agent takes actions on instances of the environment, called \textbf{states}, while following a policy $\pi$.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=10cm]{RL.png}
	\caption{Illustration of the advantage actor-critic (A2C) reinforcement learning approach \cite{gpart}\cite{DeepRL}.}
	\label{fig:RL}
\end{figure}


We denote by $S$ the set of states of the environment, $A(s)$ the set of actions that the agent can take in state $s$, and by $r: S \times A \rightarrow \mathbb{R}$ the reward function. A reinforcement learning problem can be described by a Markov decision process (MDP) $(s_t, a_t, r_{t+1})_{t \in [0,T-1]}$ \cite{gpart}, where $s_t \in S$, $a_t \in A(s_t)$ are the state and action at time $t$, while $r_{t+1} := r(s_t, a_t)$ is the reward received by the agent after action $a_t$ is performed. The time $t$ runs from 0 to $T-1$, the time step at which the episode ends.

The reinforcement learning process under study is depicted in figure~\ref{fig:RL} that shows the routes and paths of information propagation. Given a state $S_t$, the actor applies an action on $S_t$ to generate $S_{t+1}$. On the other hand, the critic side of the network estimates a value that is used to compute the advantage of the next state. This advantage is then used to reinforce or discourage the actor from taking that action the next time.


\section{Reproducing the paper results}

After a thorough lecture and understanding of the studied paper and its notions, an attempt was made to try to train the reinforcement learning (section \ref{rl4g}) models created using the GNNs defined in section \ref{gnns}, which failed because the library path to the SCOTCH's header wasn't found.

We propose the following lines to fix this problem.

\begin{lstlisting}
$ sudo apt install scotch libscotch-dev
$ cd partitioning/scotch
$ mkdir build
$ cd build ; cmake ..
$ make VERBOSE=1 
# fail because cmake doesn't find 
# /usr/include/scotch/scotch.h
$ cd build ; ccmake ..
# display advanced options (t) and correct 
# the variable SCOTCH_INCLUDE_DIR to 
# /usr/include/scotch. Then configure (c) 
# and generate (g).
\end{lstlisting}

\subsection{Minimal edge separator}\label{sec:mes}

The minimal edge separator aims to divide a graph into $n$ balanced sets/partitions while minimizing the normalized cut \ref{eq:nc-gen}, just as defined in section \ref{problem}.

The simplified version of the equation \ref{eq:nc-gen} for two sets is the following.

\begin{equation}
	Ncut(G) = \text{cut}(G) (\frac{1}{\text{vol}(V_A)}+\frac{1}{\text{vol}(V_B)})
	\label{eq:nc-simple}
\end{equation}

Where $G = (V,E)$ is a graph partitioned as $V = V_A \cup V_B$, $V_A \cap V_B = \emptyset$, and $cut(G) = \sum_{(u,v) \in E, v \in V_A, u \in V_B} 1$. In the paper's particular context the reward for the MDP (section ~\ref{rl4g}) is defined as $r_{t+1}=Ncut(S_t)-Ncut(S_{t+1})$.


\begin{figure}[ht!]
	\centering
	\includegraphics[width=9cm]{epochs.png}
	\caption{Reward growth according to epochs.}
	\label{fig:epochs}
\end{figure}

In the studied paper \cite{gpart}, the partitioning algorithm tackles the task recursively using minimal edge separators. After coarsening (compressing) the graph to a manageable size, it selects a partitioning method. During partitioning, a reliable existing method (DRL\_METIS) takes over. However, when the agent responsible for the DRL model is under training, this coarsened graph is partitioned using the DRL model itself.

\hfill

\hfill

\hfill

\hfill

\hfill

\hfill

\begin{figure}[ht!]
	\centering
	\includegraphics[width=9cm]{graph_a.png}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=5cm]{graphs.png}
	\caption{Evaluation of the partitioners on the SuiteSparse dataset. Training was performed on graphs from the SuiteSparse collection with $100 < n \le 5000$ nodes.}
	\label{fig:graphs}
\end{figure}

Even though the figure \ref{fig:epochs} in the studied paper \cite{gpart} clearly shows that the model has fully learned at around $250$ epochs with the results shown in figure \ref{fig:graphs}, our material limitations led us to train the different versions of models (DRL and DRL\_METIS \cite{gpart}) for $1$ epoch on $10k$ graphs of sizes (number of nodes) between $200$ and $5000$ on a local machine. Many tries were made to train the models and test them on the High-Performance Computers of the research laboratory Inria (Plafrim), but were unsuccessful due to the missing modules (Pytorch-geometric, Torch ...) that could not be added to PLafrim.

\newpage


\begin{figure}[ht!]
	\centering
	\subfigure[Normalized cut]{\includegraphics[width=4cm]{norm_cut.png}}
	\subfigure[Edge cut]{\includegraphics[width=4cm]{cut.png}}
	\subfigure[Partition balance]{\includegraphics[width=5cm]{balance.png}}
	\caption{Evaluation of the partitioners on the SuiteSparse dataset. Training was performed on graphs from the SuiteSparse collection with $100 < n \le 5000$ nodes (Local machine).}
	\label{fig:our_graphs}
\end{figure}


The results given by our trained models are given in figure ~\ref{fig:our_graphs}. Unfortunately with just one epoch of training, we can't verify if the results given by the paper are true or not. But we can clearly see that our trained model have a higher value for $Ncut$ than METIS and SCOTCH, the value of the cut is almost equal. But what is more important to note is that the value of the balance with very low which means that one partition is much bigger than the other one. This same problem was observed in figure ~\ref{fig:graphs}, which led us to the conclusion that there might be a few improvements to make on the reward function of the MDP.


 
\subsection{Minimal vertex separator}\label{sec:mvs}

A vertex separator is a set of nodes that, if removed from the graph, would split the graph in two unconnected subgraphs. The mathematical formula is not so different from equation \ref{eq:nc-gen} and can be simplified as following

\begin{equation}
	NS(G) = |V_S|(\frac{1}{|V_A|}+\frac{1}{|V_B|})
	\label{eq:ns-simple}
\end{equation}
Where $G_A = (V_A, E_A), G_B = (V_B, E_B)$ and $G_S = (V_S, E_S)$. Such that $V = V_A \cup V_B \cup V_S$. The reward for the MDP (section ~\ref{rl4g}) will now be defined as $r_{t+1}=NS(S_t)-NS(S_{t+1})$.

While the minimal vertex separator models were trained for $1$ epoch on $3k$ graphs of sizes (number of nodes) between $200$ and $5000$ on a local machine's CPU.

\begin{figure}[ht!]
	\centering
	\subfigure[Paper's normalized separator]{\includegraphics[width=6cm]{n_sep_paper.png}\label{fig:n_sep_paper}}
	\subfigure[Our normalized separator]{\includegraphics[width=5.5cm]{n_sep.png}\label{fig:n_sep}}
	\caption{Evaluation of the partitioners on the Delaunay dataset. Training was performed on graphs from the Delaunay collection with $100 < n \le 5000$ nodes (Local machine).}
	\label{fig:graphs_sep}
\end{figure}

\hfill

\hfill

\hfill

The studied paper claims having the results in figure~\ref{fig:n_sep_paper}, while the results we found are in figure~\ref{fig:n_sep}. Knowing that we trained only on one epoch we can't trust our DRL model performances and compare them with the results given in the paper. But we can observe that the results given by the METIS heuristic are the same in both graphs.

\section{Conclusion}

While the initial results using Deep Reinforcement Learning (DRL) with A2C and Graph Neural Networks for graph partitioning are promising, further exploration is needed. Training on diverse datasets like Delaunay triangulations and SuiteSparse matrices demonstrates the method's potential to achieve results comparable to METIS and SCOTH. However, a key limitation identified is the creation of unbalanced partitions, as shown in Figures~\ref{fig:graphs}and~\ref{fig:our_graphs}. This suggests the current reward function prioritizes achieving a good normalized cut over balanced partitioning. Incorporating a "balanced normalized cut" concept from \cite{GAP} could significantly improve the model's performance. Additionally, weight tuning might be necessary to determine the optimal balance between these objectives.

% For bibliography
\begin{thebibliography}{1}
	\bibitem{gpart}
	Alice Gatti, Zhixiong Hu†, Tess Smidt, Esmond G. Ng, Pieter Ghysels
	Graph Partitioning and Sparse Matrix Ordering using
	Reinforcement Learning and Graph Neural Networks
	\href{https://arxiv.org/abs/2104.03546}{https://arxiv.org/abs/2104.03546}
	, June 30, 2021
	\bibitem{NP-complete}
	Graph Nets: DeepMind's library for building
	graph networks in Tensorflow and Sonnet.
	\href{https://github.com/deepmind/graph\_nets}{https://github.com/deepmind/graph\_nets}.
	Accessed: 2020-05-20
	\bibitem{GAP}
	GAP: Generalizable Approximate Graph Partitioning Framework, \href{https://arxiv.org/pdf/1903.00614}{https://arxiv.org/pdf/1903.00614}.
	\bibitem{normalized-cut}
	Shi, J. and Malik, J. (2000).
	Normalized cuts and image segmentation. IEEE
	Trans. Pattern Anal. Mach. Intell., 22(8):888-905.
	\bibitem{Spectralnet}
	Shaham, U., Stanton, K., Li,
	H., Nadler, B., Basri, R., and Kluger, Y. (2018).
	Spectralnet: Spectral clustering using deep neural
	networks. arXiv preprint arXiv:1801.01587.
	\bibitem{GCN}
	Thomas N. Kipf and Max Welling. Semi-supervised classification with graph convolutional networks.
	arXiv:1609.02907 [cs.LG], 2017.
	\bibitem{SAGE}
	Will Hamilton, Zhitao Ying, and Jure Leskovec. Inductive representation learning on large graphs. In
	Advances in neural information processing systems, pages 1024-1034, 2017.
	\bibitem{GAT}
	Petar Veliˇckovi'c, Guillem Cucurull, Arantxa Casanova, Adriana Romero, Pietro Li`o, and Yoshua Bengio.
	Graph attention networks. In International Conference on Learning Representations, 2018.
	\bibitem{geometric}
	PyG Team \href{https://pytorch-geometric.readthedocs.io/en/latest/tutorial/create\_gnn.html}{https://pytorch-geometric.readthedocs.io/en/latest/\\tutorial/create\_gnn.html}, 2021.
	\bibitem{DeepRL}
	Alexander Zai and Brandon Brown. Deep reinforcement learning in action. Manning Publications, 2020.
\end{thebibliography}

\end{document}
