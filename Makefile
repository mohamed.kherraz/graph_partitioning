all: report clean
	@echo "If 'make' doesn't work, try using 'make -B'"

report: report/report.tex
	@echo "Building report.pdf..."
	pdflatex -output-directory=report report/report.tex
	biber report/report
	pdflatex -output-directory=report report/report.tex
	@echo "Build complete."

show:
	evince report/report.pdf &

clean:
	@echo "Cleaning up..."
	rm -f report/*.aux report/*.log report/*.out report/*.toc report/*.pdfsync report/*.bbl report/*.blg report/*.bcf report/*.xml
	@echo "Cleanup complete."
