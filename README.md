# Test the code

To test the given codes you can use the google collab links or run it on your computer.
I suggest you use a environment manager to avoid installing big libraries on your entire computer for no reason.

I used `Miniconda` to manage my environment because it is very light weight.

- `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`
- `chmod +x Miniconda3-latest-Linux-x86_64.sh`
- `bash Miniconda3-latest-Linux-x86_64.sh -p $HOME/miniconda3`
scroll with `enter` then write `yes` -> press `enter` again to unpack at the given location -> for the last one it is your choice (i chose no).
- `source \<path to conda\>/bin/activate` (if you didn't change the unpacking path it would be `source /home/user/miniconda/bin/activate`). 
- `conda deactivate` The previous command might start an environment.

Test if everything works by doing `conda list`.

if everything works lets create our environment :
- `conda create -n pyg python=3.11` to create the environment.
- `conda deactivate` to deactivate the environment.

now lets install the needed libraries.
- `pip install torch`
- `conda install -n pyg ipykernel --update-deps --force-reinstall`
- `pip install torchvision`
- `pip install matplotlib`
- `pip install numpy` (if the jupyter notebook shows and error of not knowing what Numpy is try doing Ctrl+Shift+P in your VSCode editor and write `interpretor` fix the python and jupyter interpreters to those of the `pyg` env and install any packages that are required.)
- `pip install pandas`
- `pip install gym`

# Commands

To compile the weekly activity report do `make` or `make report`.
If none of these work do `make -B` to force the recompilation.

To visualise the report on your screen do `make show`.


# File description
- `Image_Classification_with_PyTorch.ipynb` : Simple implementation of image classification using PyToch. (https://colab.research.google.com/drive/15oUHTzkns7iXV95JpSUlZxvXEMA3-Bgx?usp=sharing)
- `PyG_Exploration.ipynb` : Helps to understand what structure is used to store the graphs, how to load datasets and different tips and tricks to train your models more easily. (https://colab.research.google.com/drive/1ItQBPPTO2qg3HJ9kelMfmPTB-hhxEgrK?usp=sharing)
- `1_Hand_On_GNN.ipynb` : Simple introduction to PyG with node classification and example code of how to train and test your model accuracy and cost. (https://colab.research.google.com/drive/1h3-vJGRVloF5zStxL5I0rSy4ZUPNsjy8?usp=sharing)
- `2_Node_Classification.ipynb` : (https://colab.research.google.com/drive/14OvFnAXggxB8vM4e8vSURUp1TaKnovzX?usp=sharing)
- `3_Graph_Classification.ipynb` : (https://colab.research.google.com/drive/1I8a0DfQ3fI7Njc62__mVXUlcAleUclnb?usp=sharing)
- `8_Node_Classification_with_PyG_and_W&B.ipynb` : (https://colab.research.google.com/github/wandb/examples/blob/master/colabs/pyg/8_Node_Classification_(with_W&B).ipynb)
- `9_Graph_Classification_with_PyG_and_W&B.ipynb` : (https://colab.research.google.com/github/wandb/examples/blob/pyg/graph-classification/colabs/pyg/Graph_Classification_with_PyG_and_W%26B.ipynb)




link to collab for codes.
how to run codes.
check convolution for graphs
tutos in repository 
find problem of shortest path .
check spectal layer ssgconv in convolution layers
take a look at reinforced learning in pytorch.
Sageconv conmape to gcnconv
both spectral versions
chebconv
